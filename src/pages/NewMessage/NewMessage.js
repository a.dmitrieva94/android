import React from 'react'
import './NewMessage.css'
import {inject, observer} from 'mobx-react'
import Input from '../../components/Input/Input'
import Contacts from '../../components/Contacts/Contacts'
import Contact from '../../components/Contacts/Contact'


@inject("store")
@observer
class NewMessage extends React.Component{
    state = {
        hasError: false,
        contact: null,
        min: false
    }
    searchInput = React.createRef()
    textInput = React.createRef()
    validateMessage = () => {
        if(!this.state.contact){            
            this.setState({
                hasError: true,
            })
        } else {
            var div = document.createElement("div");
            div.classList.add('myMessage')
            div.innerHTML = this.textInput.current.value
            document.querySelector('.messageItem').appendChild(div);
            this.setState({textInput: ''})
        }
    }
    saveContact = () => {
        this.props.history.push('/messages/newmessage/foundcontact')
    }
    onFocusSearch = () => {
        this.props.store.stateKeyboard(this.searchInput.current, '', this.saveContact);
    }
    onFocusText = (e) => {
        this.props.store.stateKeyboard(this.textInput.current, '', this.validateMessage)
    }
    componentDidMount(){
        this.props.store.clockColor()
    }
    getFindString = () =>{
        return this.searchInput && this.searchInput.current ? this.searchInput.current.value : null
    }
    onForceChange = () => {
        this.forceUpdate()
    }
    setContact = (contact) => {
        this.setState({
            contact
        })
        this.props.store.closeKeyboard()
    }
    render(){
        const {contact} = this.state
        return(
            <div className="newMessage">
                <div className={`contactItem ${!!contact && 'active'}`}>
                    {contact 
                    ? <Contact 
                        fullName={contact.fullName} 
                        phoneNumber={contact.phoneNumber} 
                    />
                    : <>
                        <form className="searchContact">
                            <Input
                                onChange={this.onForceChange}
                                ref={this.searchInput}
                                onFocus={this.onFocusSearch}
                                placeholder='seach contact'
                            />
                        </form>
                        {!!this.getFindString() 
                        && <div className="contactPrev">
                                <Contacts 
                                    findString={this.getFindString()}
                                    onClickByContact={this.setContact}
                                />
                            </div>
                        }
                    </>
                    }
                </div>
                <div className={`messageItem ${this.props.store.isOpenKeyboard ? 'openKey' : 'closeKey'}`}>
                    {this.state.hasError &&
                        <p>Выберите контакт!</p>
                    }
                </div>
                <Input
                    className="answerBox"
                    ref={this.textInput}
                    onFocus={this.onFocusText}
                />
            </div>
        )
    }
}

export default NewMessage