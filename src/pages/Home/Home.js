import React from 'react'
import './Home.css'
import {inject, observer} from 'mobx-react'

@inject("store")
@observer
class Home extends React.Component{
    constructor(props) {
        super(props);
        this.homeContainer = React.createRef();
    }
    containerPosition = 0    
    onMouseDown = (e) => {
        this.homeContainer.current.addEventListener('mousemove', this.moveMouse)
        this.homeContainer.current.addEventListener('mouseleave', () => {
            this.rightPosition()
        })
    }
    moveMouse = (event) => {
        var direction = event.movementX < 0 ? 'right' : 'left'
        if(
            (this.containerPosition >= 0 && direction === 'left')
            || (this.containerPosition <= -700 && direction === 'right')
        ){
            return
        }
        this.containerPosition += event.movementX
        this.homeContainer.current.style.transform = 'translateX('+this.containerPosition+'px)';
    }
    rightPosition = () =>{
        const {current} = this.homeContainer
        current.removeEventListener('mousemove', this.moveMouse)
        current.style.transition = 'all 0.3s ease'
        if (-this.containerPosition < 350/2){ 
            this.containerPosition = 0
        } else if (-this.containerPosition > 350/2 && -this.containerPosition < 350+350/2){ 
            this.containerPosition = -350
        } else if (-this.containerPosition > 350+350/2){ 
            this.containerPosition = -350*2
        }
        current.style.transform = 'translateX('+this.containerPosition+'px)';
        setTimeout(() => {
            current.style.transition = ''
        }, 300)
    }
    onMouseUp = (e) => {
        this.rightPosition()
    }
    componentDidMount(){
        this.props.store.clockColor()
    }
    render(){
        const {history} = this.props
        return(
            <div className="allHome">
                <div className="homeContainer" ref={this.homeContainer} onMouseDown={this.onMouseDown} onMouseUp={this.onMouseUp}>
                    <div className="homePage">
                        <div className="row">
                            <i className="fab fa-android"></i>
                            <i className="fab fa-viber"></i>
                            <i className="fab fa-vk"></i>
                            <i className="fab fa-telegram"></i>
                            <i className="fas fa-horse"></i>
                        </div>
                        <div className="row"></div>
                        <div className="row"></div>
                        <div className="row"></div>
                        <div className="row"></div>
                        <div className="row">
                            <i className="far fa-kiss-wink-heart"></i>
                            <i className="far fa-map"></i>
                            <i className="fas fa-cookie-bite"></i>
                            <i className="fas fa-paw"></i>
                            <i className="fas fa-cog" onClick={() => {history.push('/settings')}}></i>
                        </div>
                    </div>
                    <div className="homePageTwo">
                            <div className="row"></div>
                            <div className="row"></div>
                            <div className="row"></div>
                            <div className="row"></div>
                            <div className="row"></div>
                            <div className="row">
                                <i className="fas fa-cog"></i>
                            </div>
                    </div>
                    <div className="homePageThree">
                        <div className="row"></div>
                        <div className="row">
                            <i className="far fa-kiss-wink-heart"></i>
                        </div>
                        <div className="row"></div>
                        <div className="row"></div>
                        <div className="row"></div>
                        <div className="row"></div>
                    </div>
                </div>
                <div className="row menu" onClick={() => {history.push('/settings')}}>
                    <i className="fas fa-angle-up"></i>
                </div>
                <div className="rowLast">
                    <i className="fas fa-phone" onClick={() => {history.push('/callapp/historycalls')}}></i>
                    <i className="fas fa-sms" onClick={() => {history.push('/messages')}}></i>
                    <i className="fab fa-internet-explorer"></i>
                    <i className="fas fa-camera" onClick={() => {history.push('/cameraapp')}}></i>
                    <i className="fas fa-images"></i>
                </div>
            </div>
        )
    }
}

export default Home



