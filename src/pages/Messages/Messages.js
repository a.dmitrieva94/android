import React from 'react'
import './Messages.css'
import {inject, observer} from 'mobx-react'

@inject("store")
@observer
class Messages extends React.Component{
    componentDidMount(){
        this.props.store.clockColor()
    }
    render(){
        const {history} = this.props
        return(
            <div className="messages">
                <div className="rowSms">
                    <div className="avatarContact"></div>
                    <div className="prevSms">
                        <h2 className="nameContact"></h2>
                        <p className="textSms"></p>
                    </div>
                </div>
                <div className="rowSms">
                    <div className="avatarContact"></div>
                    <div className="prevSms">
                        <h2 className="nameContact"></h2>
                        <p className="textSms"></p>
                    </div>
                </div>
                <div className="rowSms">
                    <div className="avatarContact"></div>
                    <div className="prevSms">
                        <h2 className="nameContact"></h2>
                        <p className="textSms"></p>
                    </div>
                </div>
                <div className="rowSms">
                    <div className="avatarContact"></div>
                    <div className="prevSms">
                        <h2 className="nameContact"></h2>
                        <p className="textSms"></p>
                    </div>
                </div>
                <div className="rowSms">
                    <div className="avatarContact"></div>
                    <div className="prevSms">
                        <h2 className="nameContact"></h2>
                        <p className="textSms"></p>
                    </div>
                </div>
                <div className="rowSms">
                    <div className="avatarContact"></div>
                    <div className="prevSms">
                        <h2 className="nameContact"></h2>
                        <p className="textSms"></p>
                    </div>
                </div>
                <button id="newSms" onClick={() => {history.push('/newmessage')}}><i className="fas fa-plus"></i></button>
            </div>
        )
    }
}

export default Messages