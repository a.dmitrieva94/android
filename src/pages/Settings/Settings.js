import React from 'react'
import './Settings.css'
import {inject, observer} from 'mobx-react'

@inject("store")
@observer
class Settings extends React.Component{
    componentDidMount(){
        this.props.store.clockColor('black')
    }
    render(){
        return(
            <div className="settings">
                <div className="settings_item" onClick={()=>{this.props.history.push('/settings/changepin')}}>Change your pin code</div>
                <div className="settings_item">Пункт меню</div>
            </div>
        )
    }
}

export default Settings