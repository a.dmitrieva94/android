import React from 'react'
import './ChangePin.css'
import {inject, observer} from 'mobx-react'
import Input from '../../components/Input/Input'

@inject("store")
@observer
class ChangePin extends React.Component{
    state = {
        hasErrorOldPass: false,
        hasErrorNewPass: false,
    }
    oldPassInput = React.createRef()
    newPassInput = React.createRef()
    repeatPassInput = React.createRef()

    onFocusOldPass = () => {
        this.props.store.stateKeyboard(this.oldPassInput.current, 'black', this.assertPin)
    }
    onFocusNewPass = () => {
        this.props.store.stateKeyboard(this.newPassInput.current, 'black', null)
    }
    onFocusRepeatPass = () => {
        this.props.store.stateKeyboard(this.repeatPassInput.current, 'black', this.assertPin)
    }
    assertPin = () => {
        if(!this.state.hasErrorNewPass && !this.state.hasErrorOldPass){
            this.props.store.changePassword(this.repeatPassInput.current.value)
            this.props.history.goBack()
            this.props.store.closeKeyboard()
        }
    }
    changeOldPass = (val) => {
        if(val === this.props.store.password){
            this.setState({hasErrorOldPass: false})
        } else {
            this.setState({hasErrorOldPass: true})
        }
    }
    changeNewPass = (val) =>{
        if(val === this.newPassInput.current.value){
            this.setState({hasErrorNewPass: false})
        } else {
            this.setState({hasErrorNewPass: true})
        }
    }
    componentDidMount(){
        this.props.store.clockColor('black')
    }
    render(){
        return(
            <div className="changePin">
                <div className="rowPass">
                    <label htmlFor='oldPass'>Старый пароль:</label>
                    <Input
                        type="password"
                        onChange={this.changeOldPass}
                        onFocus={this.onFocusOldPass}
                        ref={this.oldPassInput}
                        placeholder="старый пароль"
                    />
                    {this.state.hasErrorOldPass && 
                        <p>Неверный пароль!</p>            
                    }
                </div>
                <div className="rowPass">
                    <label htmlFor='newPass'>Новый пароль:</label>
                    <Input
                        type="password"
                        onFocus={this.onFocusNewPass}
                        ref={this.newPassInput}
                        placeholder="новый пароль"
                    />
                </div>
                <div className="rowPass">
                    {this.state.hasErrorNewPass && 
                        <p>Введенные пароли <br/> не совпадают!</p>            
                    }
                    <label htmlFor='repeatPass'>Повторите новый пароль:</label>
                    <Input
                        type="password"
                        onChange={this.changeNewPass}
                        onFocus={this.onFocusRepeatPass}
                        ref={this.repeatPassInput}
                        placeholder="новый пароль"
                    />
                </div>
            </div>
        )
    }
}

export default ChangePin