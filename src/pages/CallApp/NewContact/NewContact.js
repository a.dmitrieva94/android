import React from 'react'
import './NewContact.css'
import {inject, observer} from 'mobx-react'

import Input from '../../../components/Input/Input'


@inject("store")
@observer
class NewContact extends React.Component{
    state = {
        fields: {
            firstname: {
                name: 'Имя'
            },
            lastname: {
                name: 'Фамилия'
            },
            surname: {
                name: 'Отчество'
            },
            phone: {
                name: 'Телефон'
            }
        }
    }
    saveContact = () => {
        const result = Object.keys(this.refs).reduce((res, key) => {
            res[key] = this.refs[key].value
            return res;
        }, {})
        this.props.store.saveContact(result)
        this.props.history.push('/callapp/contactsbook')
        this.props.store.closeKeyboard()
    }
    onFocusSearch = (refName) => {
        this.props.store.stateKeyboard(this.refs[refName], '', this.saveContact)
    }
    render(){
        const {fields} = this.state;
        return(
            <form className="newContact">
                <div className='newContactRow'>
                    <p>Добавьте картинку</p>
                    <div className="avatarContact"><i className="fas fa-plus"></i></div>
                </div>
                {Object.keys(this.state.fields).map((key) =>
                    <div key={key} className='newContactRow'>
                        <p>{fields[key].name}</p>
                        <Input
                            ref={key}
                            onFocus={() => {this.onFocusSearch(key)}} 
                            name={key}
                        />
                    </div>
                )}                 
                <div className="saveChanges">
                    <button type="button" onClick={this.saveContact}>Сохранить</button><button onClick={(e) =>{e.preventDefault(); this.props.history.goBack()}}>Отмена</button>
                </div>
            </form>
        )
    }
}

export default NewContact