import React from 'react'
import './ContactsBook.css'
import Contacts from '../../../components/Contacts/Contacts'
import {inject, observer} from 'mobx-react'
import Input from '../../../components/Input/Input'


@inject("store")
@observer
class ContactsBook extends React.Component{
    searchInput = React.createRef()
    onFocusSearch = () => {
        this.props.store.stateKeyboard(this.searchInput.current, '', null);
    }
    getFindString = () =>{
        return this.searchInput && this.searchInput.current ? this.searchInput.current.value : ''
    }
    onForceChange = () => {
        this.forceUpdate()
    }
    render(){
        return(
            <div className="contactsBook">
                <Input
                    className="findContact"
                    onChange={this.onForceChange}
                    ref={this.searchInput}
                    onFocus={this.onFocusSearch}
                    placeholder='Find contact'
                />
                <div>
                    <Contacts findString={this.getFindString()}/>
                </div>
                <button id="newContact" onClick={() => {this.props.history.push('/callapp/newcontact')}}><i className="fas fa-plus"></i></button>
            </div>
        )
    }
}

export default ContactsBook