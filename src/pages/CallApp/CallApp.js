import React from 'react'
import './CallApp.css'
import {inject, observer} from 'mobx-react'
import {Route} from "react-router-dom";
import HistoryCalls from './HistoryCalls/HistoryCalls'
import ContactsBook from './ContactsBook/ContactsBook'
import NewContact from './NewContact/NewContact'


@inject("store")
@observer
class CallApp extends React.Component{
    componentDidMount(){
        this.props.store.clockColor()
    }
    render(){
        const callsMenu = [
            {
                icon: 'phone',
                path: '/callapp/historycalls',
                component: HistoryCalls
            },
            {
                icon: 'clock',
                path: '/callapp/numbercalls',
                component: HistoryCalls
            },
            {
                icon: 'user',
                path: '/callapp/contactsbook',
                component: ContactsBook
            },
            {
                icon: false,
                path: '/callapp/newcontact',
                component: NewContact
            }
        ]
        const {history} = this.props
        return(
                <div className="callApk">
                    {location.pathname !== '/callapp/newcontact' &&
                        <div className="callsMenu">
                            {callsMenu.map(({icon, path}) => icon ?
                                <div key={path} onClick={() => {history.push(path)}} className={location.pathname === path ? 'active' : ''}>
                                    <i className={`fas fa-${icon}`}></i>
                                </div>
                                : null
                            )} 
                        </div>
                    }
                    {callsMenu.map(({component, path}) => 
                        <Route key={path} path={path} component={component} />
                    )} 
                </div>
        )
    }
}
export default CallApp