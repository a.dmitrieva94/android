import React from 'react'
import './HistoryCalls.css'

class HistoryCalls extends React.Component{
    render(){
        return(
            <div className="historyCalls">
                <div className="rowCall">
                    <div className="avatarContact"></div>
                    <div className="contactInfo">
                        <div className="nameContact"></div>
                        <div className="timeCall"></div>
                    </div>
                    <i className="fas fa-phone"></i>
                </div>
            </div>
        )
    }
}

export default HistoryCalls