import { toJS, observable, action, computed } from 'mobx';

const BLOCK_TIMEOUT = 60000;

class Store{
    @observable isOpenKeyboard = false;
    className = '';
    @observable clockClass ='';
    keyClass = '';
    @observable isShowPasswordInput = false;
    @observable password = localStorage.getItem('password') || '0000'  
    @observable isBlockWindow = false;
    callbackByEnter = null;
    callbackByButtons = null;
    callbackByBackButton = null;
    timeout;
    @observable isShowCamera = true;
    @observable findContacts = '';
    @observable contactsBook = JSON.parse(localStorage.getItem('contactsBook')) || [
        {
            firstname: 'Alex',
            lastname: 'Pachianidi',
            surname: 'Igorevich',
            phone: '+79264347319'
        },
        {
            firstname: 'Анна',
            lastname: 'Дмитриева',
            surname: 'Сергеевна',
            phone: '89099505684'
        },
        {
            firstname: 'Зина',
            lastname: 'Che',
            surname: 'Абдурахмановна',
            phone: '+79545655544'
        }
    ]

    @computed
    get contactsList() {
        return this.contactsBook.filter((elem) => {
            return (
                elem.firstname.toLowerCase().includes(this.findContacts.toLowerCase())
                || elem.lastname.toLowerCase().includes(this.findContacts.toLowerCase())
                || elem.surname.toLowerCase().includes(this.findContacts.toLowerCase())
            )
        }).sort(function (a, b) {
            const firstSort = a.firstname || a.surname || a.lastname
            const lastSort = b.firstname || b.surname || b.lastname
            return firstSort > lastSort ? 1 : -1
        });
    }

    time_out = () => {
        document.removeEventListener('click', this.restartTimeout)
        document.addEventListener('click', this.restartTimeout) 
        this.timeout = setTimeout(() => {
            this.setBlockWindow(true)
            this.clockColor('podlogka')
        }, BLOCK_TIMEOUT)
    }
    restartTimeout = () => {
        clearTimeout(this.timeout)
        this.time_out();
    }

    @action
    setShowPasswordInput = (state = false) => {
        this.isShowPasswordInput = state
    }

    @action
    setShowCamera = (state = true) => {
        this.isShowCamera = state
        if(this.isShowCamera){
            if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                    video.srcObject = stream;
                    video.play();
                });
            }
        }
    }

    @action
    setFindContact = (findString) => {
        this.findContacts = findString
    }
    @action
    saveContact = (elem) => {
        this.contactsBook.push(elem);
        var clone = toJS(this.contactsBook);
        localStorage.setItem('contactsBook', JSON.stringify(clone))
    }
    @action
    stateKeyboard = (inputComponent, className = '', callbackByEnter = null) => {
        this.isOpenKeyboard = true
        this.inputComponent = inputComponent
        this.className = className
        this.callbackByEnter = callbackByEnter;
        className === 'black' ? this.keyClass = 'keyBlack' : this.keyClass = ' '
    };
    @action
    closeKeyboard = () => {
        this.isOpenKeyboard = false;
        this.inputComponent = null;
        this.className = '';
        this.callbackByEnter = null;
        this.keyClass = '';
    }
    @action
    clockColor = (className = '') => {
        switch(className) {
            case 'black':
                this.clockClass = 'clockRowBlack'
                break;
            case 'podlogka':
                this.clockClass = 'clockPodlogka'
                break;
            default:
                this.clockClass = ' '
        }
        this.className = className
    };

    @action
    changePassword = (newPass) => {
        localStorage.setItem('password', newPass)
        this.password = newPass
    };
    @action
    setBlockWindow = (state) => {
        this.isBlockWindow = state
        if (state) {
            document.removeEventListener('click', this.restartTimeout)
            clearTimeout(this.timeout) 
        } else {
            this.time_out();
        }
    };

}

export default Store