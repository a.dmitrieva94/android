import ReactDOM from 'react-dom'
import React from 'react'
import Layout from './components/Layout/Layout'
import Home from './pages/Home/Home'
import { BrowserRouter as Router, Route} from "react-router-dom";
import Settings from './pages/Settings/Settings'
import ChangePin from './pages/Settings/ChangePin'
import Camera from './components/Camera/Camera'
import Messages from './pages/Messages/Messages'
import NewMessage from './pages/NewMessage/NewMessage'
import CallApp from './pages/CallApp/CallApp'
import Store from './Store'
import {Provider} from 'mobx-react'

const store = new Store()

class App extends React.Component{
    render(){
        return(
            <Provider store={store}>
                <Router basename="/android">
                    <Layout>
                        <Route path="/" exact component={Home} />
                        <Route path="/settings" exact component={Settings} />
                        <Route path="/settings/changepin" component={ChangePin} />
                        <Route path="/callapp" component={CallApp} />
                        <Route path="/messages" component={Messages} />
                        <Route path="/newmessage" component={NewMessage} />
                        <Route path="/messages/newmessage/foundcontact" component={Messages} />
                        <Route path="/contactfound" component={NewMessage} />
                        <Route path="/cameraapp" exact component={Camera} />
                    </Layout>
                </Router>
            </Provider>
        )
    }
}
ReactDOM.render(<App/>, document.getElementById('root'))
