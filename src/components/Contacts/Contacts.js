import {inject, observer} from 'mobx-react'
import React from 'react'
import './Contacts.css'

import Contact from './Contact';

@inject("store")
@observer
class Contacts extends React.Component{
    componentDidUpdate(prevProps){
        if(this.props.findString !== prevProps.findString){
            this.props.store.setFindContact(this.props.findString)
        }
    }
    componentDidMount() {
        this.props.store.setFindContact(this.props.findString)
    }
    render(){
        return(
            <div className="contactList">
                {this.props.store.contactsList.map((elem) =>{
                    const fullName = elem.firstname + ' ' + elem.lastname + ' ' + elem.surname;
                    const phoneNumber = elem.phone
                    return(
                        <Contact
                            key={fullName+phoneNumber}
                            fullName={fullName} 
                            phoneNumber={phoneNumber} 
                            onClick={()=>{this.props.onClickByContact({fullName, phoneNumber})}} 
                        />
                    )
                })}
            </div>            
        )
    }
}

export default Contacts