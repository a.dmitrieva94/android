import React from 'react';

const Contact = ({fullName, phoneNumber, onClick}) => 
    <div className="rowContactPrev" onClick={onClick}>
        <div className="avatarContact"></div>
        <div className="contactInfo">
            <div className="fullName"> {fullName} </div>
            <div className="phone"> {phoneNumber} </div>
        </div>
    </div>;

export default Contact;