import React from 'react'
import {inject, observer} from 'mobx-react'
import './BlockWindow.css'
import Input from '../Input/Input'

@inject("store")
@observer
class BlockWindow extends React.Component{
    state = {
        hasError: false
    } 
    passInput = React.createRef()
    onFocus = () => {
        this.props.store.stateKeyboard(this.passInput.current, 'black', this.assertPin)
    }
    assertPin = () => {
        if(this.passInput.current.value === this.props.store.password){
            this.props.store.setBlockWindow(false)
            this.props.store.clockColor()
            this.props.store.closeKeyboard()
        } else {
            this.setState({hasError: true})
        }
    }
    render(){
        const {
            store,
        } = this.props;
        return(
            <div onClick={() => {store.setShowPasswordInput(true)}} className="podlogka">
                {store.isShowPasswordInput &&
                    <>
                        <Input 
                            ref={this.passInput}
                            onFocus={this.onFocus}
                            type="password"
                            className="write"
                        />
                        {this.state.hasError && 
                            <p className="wrong">Неверный пароль!</p>
                        }
                    </>
                }
            </div>
        )
    }
}

export default BlockWindow 