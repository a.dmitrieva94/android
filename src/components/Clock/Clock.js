import React from 'react'

class Clock extends React.Component{
    state = {
        time: '',
    }

    update = () => {
        var date = new Date(); 
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
        var minutes = date.getMinutes();
        if (minutes < 10) minutes = '0' + minutes;
        var seconds = date.getSeconds();
        if (seconds < 10) seconds = '0' + seconds;

        const time = `${hours}:${minutes}:${seconds}`

        this.setState({
            time,
        })
        setInterval(this.update, 1000);
    }
    componentDidMount(){   
        this.update(); 
    }
    render(){
        return (
            <span>
                {this.state.time}
            </span>
        )
    }
} 

export default Clock 