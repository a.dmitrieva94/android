import React from 'react'
import './Layout.css'
import Clock from '../Clock/Clock'
import BlockWindow from '../BlockWindow/BlockWindow'
import {Keyboard} from '../Keyboard/Keyboard'
import {inject, observer} from 'mobx-react'
import { withRouter } from "react-router";


@inject("store")
@observer
class Layout extends React.Component{
    componentDidMount(){
        this.props.store.time_out()
    }
    goBack = () => {
        if(this.props.store.isOpenKeyboard){
            this.props.store.closeKeyboard()
        } else if (this.props.history.location.pathname !== '/') {
            if (this.props.history.location.pathname === '/cameraapp' && !this.props.store.isShowCamera) {
                this.props.store.setShowCamera(true)
            } else{
                this.props.history.goBack()
            }
        }
        this.props.store.setShowPasswordInput()
    }
    goHome = () => {
        if (this.props.history.location.pathname !== '/') {
            this.props.history.push('/')
        } 
        if (this.props.store.isBlockWindow){
            console.log(this.props.store)
            this.props.store.setShowPasswordInput(true)
        } else {
            this.props.store.setShowPasswordInput()
            this.props.store.clockColor()
        }
        if(this.props.store.isOpenKeyboard){
            this.props.store.closeKeyboard()
        }
    }
    render(){
        return(
            <div className="container">
                <header>
                    <div className="camera"></div>
                </header>
                <div className= "content">
                    <div className={`clockRow ${this.props.store.clockClass}`}>
                        <div className="clock">
                            <Clock/>
                        </div>
                        <i className="fab fa-viber"></i>
                        <i className="fas fa-battery-quarter"></i>
                    </div>
                    {this.props.store.isBlockWindow
                        ? <BlockWindow/>
                        : this.props.children
                    }
                </div>
                {this.props.store.isOpenKeyboard &&
                    <Keyboard/>
                }
                <footer>
                    <button onClick={this.goHome} className="homeButton"></button>
                    <div onClick={this.goBack} className="backButton">
                        <i className="fas fa-chevron-circle-left"></i>
                    </div>
                </footer>
            </div>           
        )
    }   
}



export default withRouter(Layout)