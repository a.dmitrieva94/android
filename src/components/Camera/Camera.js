import React from 'react'
import {push} from "react-router-dom";
import './Camera.css'
import {inject, observer} from 'mobx-react'


@inject("store")
@observer
class Camera extends React.Component{
    drAwI = () => {
        document.getElementById('canvas').getContext('2d').drawImage(video, 0, 0, 350, 263);
        this.props.store.setShowCamera(false)
    };
    componentDidMount(){
        this.props.store.clockColor('black')
        if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            console.log('123')
            navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                video.srcObject = stream;
                video.play();
            }).catch((e) => {
                console.log(e)
            })
        }
    }
    render(){
        return(
            <>
            {this.props.store.isShowCamera &&
                <div className="cameraApp">
                    <video id="video" width="350" height="470" autoPlay></video>
                    <button
                        id="snap"
                        onClick={this.drAwI}
                    />
                </div>                
                }
                <div className="canvasLayout">
                    <canvas id="canvas" width="350" height="450"></canvas>                
                </div>
            </>
        )
    }
}

export default Camera 