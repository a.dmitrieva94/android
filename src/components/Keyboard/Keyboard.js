import React from 'react'
import './Keyboard.css'
import {inject, observer} from 'mobx-react'

@inject("store")
@observer
export class Keyboard extends React.Component{
    state = { 
        uppercase: false,
        symbolsShow: false,
    }
    clickBySpan = (e) => {
        if(e.target.tagName === 'SPAN'){
            this.props.store.inputComponent.add(e.target.innerHTML)
        }
    }
    clickByEnter = () => {
        const {callbackByEnter} = this.props.store;
        if(callbackByEnter){
            callbackByEnter()
        }
    }
    clickByBackspace = () => {
        const {inputComponent} = this.props.store;
        if(inputComponent){
            inputComponent.remove()
        }
    }
    upperSymbol = () =>{
        this.setState({
            uppercase: !this.state.uppercase
        })
    }
    changeLayout = () => {
        this.setState({
            symbolsShow: !this.state.symbolsShow
        })
    }
    render(){
        const {
            uppercase,
            symbolsShow,
        } = this.state;

        let wrapClassName = ''
        if(uppercase){
            wrapClassName+='uppercase'
        }
        if(symbolsShow){
            wrapClassName+=' symbolsShow'
        }

        return(
            <ul className={`keyboard ${this.props.store.keyClass} ${wrapClassName}`} id="engSymbol" onClick={this.clickBySpan}>
                <li className="firstitem"><span className="letter">1</span><span className="symbolBtn">1</span></li>
                <li><span className="letter">2</span><span className="symbolBtn">2</span></li>
                <li><span className="letter">3</span><span className="symbolBtn">3</span></li>
                <li><span className="letter">4</span><span className="symbolBtn">4</span></li>
                <li><span className="letter">5</span><span className="symbolBtn">5</span></li>
                <li><span className="letter">6</span><span className="symbolBtn">6</span></li>
                <li><span className="letter">7</span><span className="symbolBtn">7</span></li>
                <li><span className="letter">8</span><span className="symbolBtn">8</span></li>
                <li><span className="letter">9</span><span className="symbolBtn">9</span></li>
                <li className="lastitem"><span className="letter">0</span><span className="symbolBtn">0</span></li>
                <li className="firstitem"><span className="letter">q</span><span className="symbolBtn">!</span></li>
                <li><span className="letter">w</span><span className="symbolBtn">#</span></li>
                <li><span className="letter">e</span><span className="symbolBtn">$</span></li>
                <li><span className="letter">r</span><span className="symbolBtn">%</span></li>
                <li><span className="letter">t</span><span className="symbolBtn">@</span></li>
                <li><span className="letter">y</span><span className="symbolBtn">?</span></li>
                <li><span className="letter">u</span><span className="symbolBtn">*</span></li>
                <li><span className="letter">i</span><span className="symbolBtn">-</span></li>
                <li><span className="letter">o</span><span className="symbolBtn">+</span></li>
                <li className="lastitem"><span className="letter">p</span><span className="symbolBtn">=</span></li>
                <li className="firstitem2"><span className="letter">a</span><span className="symbolBtn">;</span></li>
                <li><span className="letter">s</span><span className="symbolBtn">:</span></li>
                <li><span className="letter">d</span><span className="symbolBtn">'</span></li>
                <li><span className="letter">f</span><span className="symbolBtn">_</span></li>
                <li><span className="letter">g</span><span className="symbolBtn">`</span></li>
                <li><span className="letter">h</span><span className="symbolBtn">~</span></li>
                <li><span className="letter">j</span><span className="symbolBtn">"</span></li>
                <li><span className="letter">k</span><span className="symbolBtn">(</span></li>
                <li className="lastitem2"><span className="letter">l</span><span className="symbolBtn">)</span></li>
                <li className="symbol firstitem3" onClick={this.upperSymbol}><i className="fas fa-sort-alpha-up"></i></li>
                <li><span className="letter">x</span><span className="symbolBtn">[</span></li>
                <li><span className="letter">z</span><span className="symbolBtn">]</span></li>
                <li><span className="letter">c</span><span className="symbolBtn">{'{'}</span></li>
                <li><span className="letter">v</span><span className="symbolBtn">{'}'}</span></li>
                <li><span className="letter">b</span><span className="symbolBtn">{'<'}</span></li>
                <li><span className="letter">n</span><span className="symbolBtn">{'>'}</span></li>
                <li><span className="letter">m</span><span className="symbolBtn">№</span></li>
                <li className="symbol lastitem3" onClick={this.clickByBackspace}><i className="fas fa-backspace"></i></li>
                <li className="symbol3 firstitem4" onClick={this.changeLayout}>123</li>
                <li><i className="far fa-smile"></i></li>                    
                <li className="symbol2"><span className="letter">,</span><span className="symbolBtn">,</span></li>
                <li className="space"><span className="letter"> </span><span className="symbolBtn"> </span></li>
                <li className="symbol3"><span className="letter">.</span><span className="symbolBtn">.</span></li>
                <li className="symbol4 lastitem4" onClick={this.clickByEnter}><i className="fas fa-level-down-alt"></i></li>
            </ul>
        )
    }
}