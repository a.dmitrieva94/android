import React from 'react'

class Input extends React.Component{
    state = {
        value: '',
    }
    onChange = (e) => {
        this.setState({
            value: e.target.value
        })
    }
    add = (symbol) => {
        this.setState({
            value: this.state.value+symbol
        })
    }
    remove = () => {
        this.setState({
            value: this.state.value.slice(0, -1)
        })
    }
    get value() {
        return this.state.value
    }
    componentDidUpdate(prevProps, prevState){
        if(this.state.value !== prevState.value && this.props.onChange){
            this.props.onChange(this.state.value)
        }
    }
    render(){
        return(
            <input 
                type={this.props.type || 'text'}
                placeholder={this.props.placeholder}
                className={this.props.className}
                onFocus={this.props.onFocus}
                value={this.state.value}
                onChange={this.onChange}
            />
        )
    }
}

export default Input
